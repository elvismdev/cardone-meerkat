/**
 * Created by l3yr0y on 5/13/15.
 */

 var today = new Date();
 var date = today.getFullYear() + '-' + (parseInt(today.getMonth()) + 1) + '-' + today.getDate();

 var testCodes = ['apptest', 'test10', 'test'];

// COUPONS

// WITNation.com
var witnationCoupons = {
    'http_method': 'https://',
    'domain': 'grantcardonetv.com',
    'version': 'v2',
    'consumer_key': 'ck_b0fd7ac1322e8e23dd0775da34b6e899', // From WP user profile configuration
    'customer_secret': 'cs_fd46d7d0fff710cc0d5f46134cc2cb34', // From WP user profile configuration
    'request': '/wc-api/v2/coupons',
    'filter': '?filter[meta_key]=usage_limit&filter[meta_value]=0',
    'result': {}
};

// GrantCardone.com
var cardoneCoupons = {
    'http_method': 'http://',
    'domain': 'www.grantcardone.com',
    'version': 'v2',
    'customer_secret': 'cs_cefb91e1ea19a54a18c085702b0fe8b1', // From WP user profile configuration
    'request': '/wc-api/v2/coupons',
    'filter': '?filter[meta_key]=usage_limit&filter[meta_value]=0',
    'oauth': {
        'oauth_consumer_key': 'ck_ac46a54f69559bd03dc5c659f3753bb8', // From WP user profile configuration
        'oauth_nonce': '',
        'oauth_signature_method': 'HMAC-SHA256',
        'oauth_timestamp': Math.round((today).getTime() / 1000)
    },
    'result': {}
};